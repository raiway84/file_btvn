import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Exercise3 {
    //    Question 1:
//    In ra thông tin Exam thứ 1 và property create date sẽ được format theo định
//    dạng vietnamese
    public void Question1(Exam exam1) {
        Locale locale = new Locale("vi", "VN");
        String format = "EEEE, dd MMMM yyyy, h:mm a";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, locale);
        String time = simpleDateFormat.format(exam1.createDate);
        System.out.println("createDate: " + time);
    }

    //    --------------------------------------------------------------------------------
//    Question 2:
//    In ra thông tin: Exam đã tạo ngày nào theo định dạng
//    Năm – tháng – ngày – giờ – phút – giây
    public void Question2(Exam exam2) {
        String format = "yyyy/MM/dd hh:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        String time = simpleDateFormat.format(exam2.createDate);
        System.out.println("createDate: " + time);
    }

    //    ------------------------------------------------------------------------------------
//    Question 3_4:
//    Chỉ in ra tháng và năm của create date property trong Question 2
    public void Question3_4(Exam exam2) {
        String format = "dd/MM/yyyy hh:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        String time = simpleDateFormat.format(exam2.createDate);
        System.out.println("createDate: " + time);
        int month = simpleDateFormat.getCalendar().get(Calendar.MONTH) + 1;
        int year = simpleDateFormat.getCalendar().get(Calendar.YEAR);
        System.out.println("Month: " + month);
        System.out.println("Year: " + year);
    }

    //        ---------------------------------------------------------------------------------
//        Question 5:
//        Chỉ in ra "MM-DD" của create date trong Question 2
    public void Question5(Exam exam3) {
        String format = "MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        String time = simpleDateFormat.format(exam3.createDate);
        System.out.println("createDate: " + time);
    }


}
