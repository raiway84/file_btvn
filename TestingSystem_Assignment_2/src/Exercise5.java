import java.util.Arrays;
import java.util.Scanner;

public class Exercise5 {

    //    Question 1:
//    Viết lệnh cho phép người dùng nhập 3 số nguyên vào chương trình
    public void Question1() {
        int number1, number2, number3;
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập vào Number1: ");
        number1 = sc.nextInt();
        System.out.println("Nhập vào Number2: ");
        number2 = sc.nextInt();
        System.out.println("Nhập vào Number3: ");
        number3 = sc.nextInt();
        System.out.println("Giá trị vừa nhập là:" + "\n" +
                "Number1 = " + number1 + "\n" +
                "Number2 = " + number2 + "\n" +
                "Number3 = " + number3 + "\n");
    }

    //    ------------------------------------------------------------------------------------
//    Question 2:
//    Viết lệnh cho phép người dùng nhập 2 số thực vào chương trình
    public void Question2() {
        float number1, number2, number3;
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập vào Number1: ");
        number1 = sc.nextFloat();
        System.out.println("Nhập vào Number2: ");
        number2 = sc.nextFloat();
        System.out.println("Nhập vào Number3: ");
        number3 = sc.nextFloat();
        System.out.println("Giá trị vừa nhập là:" + "\n" +
                "Number1 = " + number1 + "\n" +
                "Number2 = " + number2 + "\n" +
                "Number3 = " + number3 + "\n");
    }

    //    ----------------------------------------------------------------------------------------
//    Question 3:
//    Viết lệnh cho phép người dùng nhập họ và tên
    public void Question3() {
        String hoten;
        Scanner sc = new Scanner(System.in);
        System.out.println("Vui lòng nhập họ và tên: ");
        hoten = sc.nextLine();
        System.out.println("Họ tên vừa nhập là: " + hoten);
    }

    //    -------------------------------------------------------------------------------------------
//    Question 4:
//    Viết lệnh cho phép người dùng nhập vào ngày sinh nhật của họ
    public void Question4() {
        System.out.println("Nhập vào ngày sinh nhật của bạn: ");
        Scanner sc = new Scanner(System.in);
        String birthday = sc.nextLine();
        System.out.println("Ngày sinh nhật của bạn là: " + birthday);
    }

    //    --------------------------------------------------------------------------------------------------
//    Question 5:
//    Viết lệnh cho phép người dùng tạo account (viết thành method)
//    Đối với property Position, Người dùng nhập vào 1 2 3 4 5 và vào
//    chương trình sẽ chuyển thành Position.Dev, Position.Test,
//    Position.ScrumMaster, Position.PM
    public void Question5() {
        Scanner sc = new Scanner(System.in);
        System.out.println("1. Dev");
        System.out.println("2. Test");
        System.out.println("3. Scrum_Master");
        System.out.println("4. PM");
        System.out.println("Mời bạn chọn phòng ban");
        PositionName positionName;
        int choose = sc.nextInt();
        switch (choose) {
            case 1:
                positionName = PositionName.Dev;
                break;
            case 2:
                positionName = PositionName.Test;
                break;
            case 3:
                positionName = PositionName.Scrum_Master;
                break;
            case 4:
            default:
                positionName = PositionName.PM;
        }
        System.out.println("Phòng ban bạn vừa chọn là: " + positionName.name());
    }

//    -----------------------------------------------------------------------------------------
//    Question 6:
//    Viết lệnh cho phép người dùng tạo department (viết thành method)
    public void Question6(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Vui lòng nhập vào thông tin Department: ");
        System.out.println("ID: ");
        int id = sc.nextInt();

        System.out.println("Name: ");
        String name = sc.next();

        System.out.println(id +" "+ name);
    }

//    ------------------------------------------------------------------------------------------
//    Question 7:
//    Nhập số chẵn từ console
    public void Question7(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập vào số chắn: ");
        int numberChan = sc.nextInt();
        while (numberChan%2 != 0){
            System.out.println("Số vừa nhập vào không phải số chắn, mời nhập lại: ");
            numberChan = sc.nextInt();
        }
        System.out.println("Số chắn vừa nhập là: " +numberChan);
    }


}
