public class Exercise1 {
/*
        If_Else

        Question 1:
        - Kiểm tra account thứ 2
        - Nếu không có phòng ban (tức là department == null) thì sẽ in ra text
        Nhân viên này chưa có phòng ban"
        - Nếu không thì sẽ in ra text "Phòng ban của nhân viên này là ..."
*/

    public void Question1(Account account2) {
        System.out.println("Chạy bài 1");
        if (account2.departmentId == null) {
            System.out.println("Nhân viên này chưa có phòng ban");
        } else {
            System.out.println("Phòng ban của nhân viên này là: " + account2.departmentId.departmentName);
        }
    }

    //    ------------------------------------------------------------------------------------------------------------
    /*
        Question 2:
        Kiểm tra account thứ 2
        - Nếu không có group thì sẽ in ra text "Nhân viên này chưa có group"
        - Nếu có mặt trong 1 hoặc 2 group thì sẽ in ra text "Group của nhân viên
        này là Java Fresher, C# Fresher"
        - Nếu có mặt trong 3 Group thì sẽ in ra text "Nhân viên này là người
        quan trọng, tham gia nhiều group"
        - Nếu có mặt trong 4 group trở lên thì sẽ in ra text "Nhân viên này là
        người hóng chuyện, tham gia tất cả các group"
    */
    public void Question2(Account account2) {
        System.out.println("Chạy bài 2");
        int size = account2.groups.length;
        if (size == 0) {
            System.out.println("Nhân viên này chưa có group");
        } else if (size == 1 || size == 2) {
            System.out.println("Group của nhân viên này là Java Fresher, C# Fresher");
        } else if (size == 3) {
            System.out.println("Nhân viên này là người quan trọng, tham gia nhiều group");
        } else {
            System.out.println("Nhân viên này là người hóng chuyện, tham gia tất cả các group");
        }
    }
//    ------------------------------------------------------------------------------------------------------------

    /*
        Question 3:
        - Sử dụng toán tử ternary để làm Question 1
    */
    public void Question3(Account account2) {
        System.out.println("Chạy bài 3");
        System.out.println((account2.departmentId == null) ? "Nhân viên này chưa có phòng ban" : "Phòng ban của nhân viên này là: "
                + account2.departmentId.departmentName);
    }
//    ----------------------------------------------------------------------------------------------------------

    /*
        Question 4:
        Sử dụng toán tử ternary để làm yêu cầu sau:
        - Kiểm tra Position của account thứ 1
        - Nếu Position = Dev thì in ra text "Đây là Developer"
        - Nếu không phải thì in ra text "Người này không phải là Developer"
    */
    public void Question4(Account account1) {
        System.out.println("Chạy bài 4");
        System.out.println((account1.positionId.positionName == PositionName.Dev) ? "Đây là Developer" : "Người này không phải là Developer");
    }
//    ---------------------------------------------------------------------------------------------------------

    /*
    Switch_case
    Question 5:
    Lấy ra số lượng account trong nhóm thứ 1 và in ra theo format sau:
    - Nếu số lượng account = 1 thì in ra "Nhóm có một thành viên"
    - Nếu số lượng account = 2 thì in ra "Nhóm có hai thành viên"
    - Nếu số lượng account = 3 thì in ra "Nhóm có ba thành viên"
    - Còn lại in ra "Nhóm có nhiều thành viên"
*/
    public void Question5(Account[] accounts) {
        System.out.println("Chạy bài 5");
        int size = accounts.length;
        switch (size) {
            case 1:
                System.out.println("Nhóm có một thành viên");
                break;
            case 2:
                System.out.println("Nhóm có hai thành viên");
                break;
            case 3:
                System.out.println("Nhóm có ba thành viên");
                break;
            default:
                System.out.println("Nhóm có nhiều thành viên");
        }
    }
//    --------------------------------------------------------------------------------------------------------

    /*
        Question 6:
        - Sử dụng switch case để làm lại Question 2
    */
    public void Question6(Account account2) {
        System.out.println("Chạy bài 6");
        int size1 = account2.groups.length;
        switch (size1) {
            case 0:
                System.out.println("Nhân viên này chưa có group");
                break;
            case 1:
            case 2:
                System.out.println("Group của nhân viên này là Java Fresher, C# Fresher");
                break;
            case 3:
                System.out.println("Nhân viên này là người quan trọng, tham gia nhiều group");
                break;
            default:
                System.out.println("Nhân viên này là người hóng chuyện, tham gia tất cả các group");
        }
    }
//    --------------------------------------------------------------------------------------------------------

    /*
        Question 7:
        - Sử dụng switch case để làm lại Question 4
    */
    public void Question7(Account account1) {
        System.out.println("Chạy bài 7");
        PositionName name = account1.positionId.positionName = PositionName.Dev;
        switch (name) {
            case Dev:
                System.out.println("Đây là Developer");
                break;
            default:
                System.out.println("Người này không phải là Developer");
        }

    }
//    ---------------------------------------------------------------------------------------------------------

    /*
    for_each
    Question 8:
    - In ra thông tin các account bao gồm: Email, FullName và tên phòng ban của
    họ
*/
    public void Question8(Account[] accounts) {
        for (Account account : accounts) {
            System.out.println("Email: " + account.email
                    + " Fullname: " + account.fullName
                    + " DepartmentName: " + account.departmentId.departmentName);
//            sử dụng for each
        }
    }

    //    -------------------------------------------------------------------------------------------------------------
/*
    Question 9:
    - In ra thông tin các phòng ban bao gồm: id và name
*/
    public void Question9(Position[] positions) {
        int length = positions.length;
        for (int i = 0; i < length; i++) {
            System.out.println("positionId: " + positions[i].positionId + " positionName: " + positions[i].positionName);
//            sử dụng for (i)
        }
    }

    /*
     ----------------------------------------------------------------------------------------------------------------
     for (i)
     Question 10:
     - In ra thông tin các account bao gồm: Email, FullName và tên phòng ban của
       họ theo định dạng như sau:
     - Thông tin account thứ 1 là:
     - Email: NguyenVanA@gmail.com
     - Full name: Nguyễn Văn A
     - Phòng ban: Sale

     - Thông tin account thứ 2 là:
     - Email: NguyenVanB@gmail.com
     - Full name: Nguyễn Văn B
     - Phòng ban: Marketting
    */
    public void Question10(Account[] accounts) {
        int length = accounts.length;
        System.out.println("Thông tin Account1 và Account2: ");
        for (int i = 0; i < length; i++) {
            System.out.println("Email: " + accounts[i].email + " FullName: " + accounts[i].fullName + " DepartmentName: "
                    + accounts[i].departmentId.departmentName);
        }
    }

    /*
        ------------------------------------------------------------------------------------------------------------------
        Question 11:
        - In ra thông tin các phòng ban bao gồm: id và name theo định dạng sau:
        - Thông tin department thứ 1 là:
        - Id: 1
        - Name: Sale
        - Thông tin department thứ 2 là:
        - Id: 2
        - Name: Marketing
    */
    public void Question11(Department[] departments) {
        int length = departments.length;
        System.out.println("Thông tin Account1 và Account2: ");
        for (int i = 0; i < length; i++) {
            System.out.println("Id: " + departments[i].departmentId + " Name: " + departments[i].departmentName);
        }
    }


//    ------------------------------------------------------------------------------------------------------------

    //    Question 13:
//    In ra thông tin tất cả các account ngoại trừ account thứ 2
    public void Question13(Account[] accounts) {
        int length = accounts.length;
        System.out.println("Thông tin Account: ");
        for (int i = 0; i < length; i++) {
            if (accounts[i].accountId != 2) {
                System.out.println("Id: " + accounts[i].accountId + " Email: " + accounts[i].email
                        + " FullName: " + accounts[i].fullName
                        + " DepartmentName: " + accounts[i].departmentId.departmentName);
            }
        }
    }

    //--------------------------------------------------------------------------------------------------------

//    Question 14:
//    In ra thông tin tất cả các account có id < 4
    public void Question14(Account[] accounts) {
    int length = accounts.length;
    System.out.println("Thông tin Account: ");
        for (int i = 0; i < length; i++) {
            if (accounts[i].accountId < 4) {
                System.out.println("Id: " + accounts[i].accountId + " Email: " + accounts[i].email
                        + " FullName: " + accounts[i].fullName
                        + " DepartmentName: " + accounts[i].departmentId.departmentName);
        }
    }
}

//    -----------------------------------------------------------------------------------------------------------

//    Question 15:
//    In ra các số chẵn nhỏ hơn hoặc bằng 20

    public void Question15() {
        System.out.println("Các số chẵn nhỏ hơn hoặc bằng 20: ");
        for (int i = 0; i <= 20; i += 2) {
            System.out.println(i);
        }
    }

/*
    ------------------------------------------------------------------------------------------------------------
    WHILE
    Question 16:
    Làm lại các Question ở phần FOR bằng cách sử dụng WHILE kết hợp với
    lệnh break, continue
*/

    public void Question16() {
        int i = 0;
        while (i <= 10) {
            System.out.println(i);
            i++;
            if (i == 5) {
                break;
            }
        }
    }

/*
    ------------------------------------------------------------------------------------------------------------
    DO-WHILE
    Question 17:
    Làm lại các Question ở phần FOR bằng cách sử dụng DO-WHILE kết hợp với
    lệnh break, continue
*/

    public void Question17() {
        int i = 0;

        do {
            i++;

            if (i % 2 == 0) {
                continue;
            }

            System.out.println("Số lẻ: " + i);
        } while (i < 10);
    }
}

