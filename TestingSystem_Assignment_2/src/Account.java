import java.util.Date;

public class Account {
    int accountId;
    String email;
    String userName;
    String fullName;
    Department departmentId; // thuộc tính khóa ngoại
    Position positionId;
    Date createDate;
    Group[] groups;
}
