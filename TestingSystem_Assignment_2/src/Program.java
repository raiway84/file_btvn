import java.util.Date;

public class Program {
    public static void main(String[] args) {
//        1.Khởi tạo Department
        Department department1 = new Department();
        department1.departmentId = 1;
        department1.departmentName = "Sale";

        Department department2 = new Department();
        department2.departmentId = 2;
        department2.departmentName = "Marketting";

        Department department3 = new Department();
        department3.departmentId = 3;
        department3.departmentName = "C";

        Department department4 = new Department();
        department4.departmentId = 4;
        department4.departmentName = "D";

//        Mảng Department
        Department[] departments = {department1, department2};
/*
----------------------------------------------------
        2.Khởi tạo Position
*/
        Position position1 = new Position();
        position1.positionId = 1;
        position1.positionName = PositionName.Dev;

        Position position2 = new Position();
        position2.positionId = 2;
        position2.positionName = PositionName.PM;

        Position position3 = new Position();
        position3.positionId = 3;
        position3.positionName = PositionName.Test;

        Position position4 = new Position();
        position4.positionId = 4;
        position4.positionName = PositionName.Scrum_Master;

//        Mảng Position
        Position[] positions = {position1, position2, position3};
/*
---------------------------------------------------
        3.Khởi tạo Account
*/
        Account account1 = new Account();
        account1.accountId = 1;
        account1.email = "NguyenVanA@gmail.com";
        account1.userName = "a1";
        account1.fullName = "Nguyen Van A";
        account1.departmentId = department1; //khóa ngoại
        account1.positionId = position1;
        account1.createDate = new Date();

        Account account2 = new Account();
        account2.accountId = 2;
        account2.email = "NguyenVanB@gmail.com";
        account2.userName = "a2";
        account2.fullName = "Nguyen Van B";
        account2.departmentId = department2; //khóa ngoại
        account2.positionId = position2;
        account2.createDate = new Date();

        Account account3 = new Account();
        account3.accountId = 3;
        account3.email = "zyz@gmail.com";
        account3.userName = "a3";
        account3.fullName = "Nguyen Van C";
        account3.departmentId = department3; //khóa ngoại
        account3.positionId = position3;
        account3.createDate = new Date();

        Account account4 = new Account();
        account4.accountId = 4;
        account4.email = "zyvb@gmail.com";
        account4.userName = "a4";
        account4.fullName = "Nguyen Van D";
        account4.departmentId = department4; //khóa ngoại
        account4.positionId = position4;
        account4.createDate = new Date();

        //        Mảng Account
        Account[] accounts1 = {account1, account2, account3};
        Account[] accounts2 = {account1, account2};
        Account[] accounts3 = {account1, account2, account3, account4};

// --Sử dụng while
// int size = accounts1.length;
// while (i < size){
//     System.out.println("Id cách 3: " + account.accountId);
//     i++;
// }

/*
--------------------------------------------------
        4.Khởi tạo Group
*/
        Group group1 = new Group();
        group1.groupId = 1;
        group1.groupName = "A1";
        group1.creatorId = account1;
        group1.createDate = new Date();

        Group group2 = new Group();
        group2.groupId = 2;
        group2.groupName = "A2";
        group2.creatorId = account2;
        group2.createDate = new Date();

        Group group3 = new Group();
        group3.groupId = 3;
        group3.groupName = "A3";
        group3.creatorId = account3;
        group3.createDate = new Date();

        //      Mảng Group
        Group[] dsGroup = {group1, group2, group3};
/*
----------------------------------------------------
        5.Khởi tạo GroupAccount
*/
        GroupAccount groupAccount1 = new GroupAccount();
        groupAccount1.groupId = group1;
        groupAccount1.accountId = account1;
        groupAccount1.joinDate = new Date();

        GroupAccount groupAccount2 = new GroupAccount();
        groupAccount2.groupId = group2;
        groupAccount2.accountId = account2;
        groupAccount2.joinDate = new Date();
        account2.groups = dsGroup;

        GroupAccount groupAccount3 = new GroupAccount();
        groupAccount3.groupId = group3;
        groupAccount3.accountId = account3;
        groupAccount3.joinDate = new Date();
/*
-------------------------------------------------------
        6.Khởi tạo TypeQuestion
*/
        TypeQuestion typeQuestion1 = new TypeQuestion();
        typeQuestion1.typeId = 1;
        typeQuestion1.typeName = TypeName.Essay;

        TypeQuestion typeQuestion2 = new TypeQuestion();
        typeQuestion2.typeId = 2;
        typeQuestion2.typeName = TypeName.Essay;

        TypeQuestion typeQuestion3 = new TypeQuestion();
        typeQuestion3.typeId = 3;
        typeQuestion3.typeName = TypeName.Multiple_Choice;
/*
-------------------------------------------------------------
        7.Khởi tạo CategoryQuestion
*/
        CategoryQuestion categoryQuestion1 = new CategoryQuestion();
        categoryQuestion1.categoryId = 1;
        categoryQuestion1.categoryName = CategoryName.Java;

        CategoryQuestion categoryQuestion2 = new CategoryQuestion();
        categoryQuestion2.categoryId = 2;
        categoryQuestion2.categoryName = CategoryName.NET;

        CategoryQuestion categoryQuestion3 = new CategoryQuestion();
        categoryQuestion3.categoryId = 3;
        categoryQuestion3.categoryName = CategoryName.SQL;
/*
-----------------------------------------------------------
        8.Khởi tạo Question
*/
        Question question1 = new Question();
        question1.questionId = 1;
        question1.content = "Trái đất hình gì?";
        question1.categoryId = categoryQuestion1;
        question1.typeId = typeQuestion1;
        question1.creatorId = account1;
        question1.createDate = new Date();

        Question question2 = new Question();
        question2.questionId = 2;
        question2.content = "Trái đất hình gì?";
        question2.categoryId = categoryQuestion2;
        question2.typeId = typeQuestion2;
        question2.creatorId = account2;
        question2.createDate = new Date();

        Question question3 = new Question();
        question3.questionId = 3;
        question3.content = "Trái đất hình gì?";
        question3.categoryId = categoryQuestion3;
        question3.typeId = typeQuestion3;
        question3.creatorId = account3;
        question3.createDate = new Date();
/*
-------------------------------------------------
        9.Khởi tạo Answer
*/
        Answer answer1 = new Answer();
        answer1.answerId = 1;
        answer1.content = "Trái đất hình cầu";
        answer1.questionId = question1;

        Answer answer2 = new Answer();
        answer2.answerId = 2;
        answer2.content = "Trái đất hình cầu";
        answer2.questionId = question2;

        Answer answer3 = new Answer();
        answer3.answerId = 3;
        answer3.content = "Trái đất hình cầu";
        answer3.questionId = question3;
/*
----------------------------------------------------
        10.Khởi tạo Exam
*/
        Exam exam1 = new Exam();
        exam1.examId = 1;
        exam1.code = "AB1";
        exam1.title = "ExamFinal1";
        exam1.categoryId = categoryQuestion1;
        exam1.duration = "45 phút";
        exam1.creatorId = account1;
        exam1.createDate = new Date();

        Exam exam2 = new Exam();
        exam2.examId = 2;
        exam2.code = "AB2";
        exam2.title = "ExamFinal2";
        exam2.categoryId = categoryQuestion1;
        exam2.duration = "60 phút";
        exam2.creatorId = account2;
        exam2.createDate = new Date();

        Exam exam3 = new Exam();
        exam3.examId = 3;
        exam3.code = "AB3";
        exam3.title = "ExamFinal3";
        exam3.categoryId = categoryQuestion1;
        exam3.duration = "90 phút";
        exam3.creatorId = account3;
        exam3.createDate = new Date();

        Exam[] exams = {exam1, exam2, exam3};
/*
---------------------------------------------------------
        11.Khởi tạo ExamQuestion
*/
        ExamQuestion examQuestion1 = new ExamQuestion();
        examQuestion1.examId = exam1;
        examQuestion1.questionId = question1;

        ExamQuestion examQuestion2 = new ExamQuestion();
        examQuestion2.examId = exam2;
        examQuestion2.questionId = question2;

        ExamQuestion examQuestion3 = new ExamQuestion();
        examQuestion3.examId = exam3;
        examQuestion3.questionId = question3;
/*
-----------------------------------------------------------
        In giá trị
*/
//        System.out.println(account2.positionId.positionId);

        /*
-----------------------------------------------------------
        Làm exercise1
*/
        Exercise1 exercise1 = new Exercise1();
//        exercise1.Question1(account2);
//        exercise1.Question2(account2);
//        exercise1.Question3(account2);
//        exercise1.Question4(account1);
//        exercise1.Question5(accounts1);
//        exercise1.Question6(account2);
//        exercise1.Question7(account1);
//        exercise1.Question8(accounts1);
//        exercise1.Question9(positions);
//        exercise1.Question10(accounts2);
//        exercise1.Question11(departments);
//        exercise1.Question13(accounts3);
//        exercise1.Question14(accounts3);
//        exercise1.Question15();
//        exercise1.Question16();
//        exercise1.Question17();

//        -----------------------------------------------------------------------------------
//        Làm exercise2
        Exercise2 exercise2 = new Exercise2();
//        exercise2.Question1();
//        exercise2.Question2();
//        exercise2.Question3();
//        exercise2.Question4();
//        exercise2.Question5();
//        exercise2.Question6(accounts3);

//        -------------------------------------------------------------------------------------
//        Làm exercise3
        Exercise3 exercise3 = new Exercise3();
//        exercise3.Question1(exam1);
//        exercise3.Question2(exam2);
//        exercise3.Question3_4(exam2);
//        exercise3.Question5(exam3);

//---------------------------------------------------------------------------------
//        Làm exercise5
        Exercise5 exercise5 = new Exercise5();
//        exercise5.Question1();
//        exercise5.Question2();
//        exercise5.Question3();
//        exercise5.Question4();
//        exercise5.Question5();
//        exercise5.Question6();
//        exercise5.Question7();

    }
}
