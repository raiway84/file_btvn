import java.text.SimpleDateFormat;
import java.util.Date;

public class Exercise2 {
//    Question 1:
//    Khai báo 1 số nguyên = 5 và sử dụng lệnh System out printf để in ra số
//    nguyên đó
    public void Question1(){
        int a = 5;
        System.out.printf("%d", a);
    }

//    -------------------------------------------------------------------------------------
//    Question 2:
//    Khai báo 1 số nguyên = 100 000 000 và sử dụng lệnh System out printf để in
//    ra số nguyên đó thành định dạng như sau: 100,000,000
    public void Question2(){
        int b = 100_000_000;
        System.out.printf("%,d", b);
    }

//    -----------------------------------------------------------------------------------------
//    Question 3:
//    Khai báo 1 số thực = 5,567098 và sử dụng lệnh System out printf để in ra số
//    thực đó chỉ bao gồm 4 số đằng sau
    public void Question3(){
        double c = 5.567098;
        System.out.printf("%.4f", c);
    }

//    ------------------------------------------------------------------------------------------
//    Question 4:
//    Khai báo Họ và tên của 1 học sinh và in ra họ và tên học sinh đó theo định
//    dạng như sau:
//    Họ và tên: "Nguyễn Văn A" thì sẽ in ra trên console như sau:
//    Tên tôi là "Nguyễn Văn A" và tôi đang độc thân.
    public void Question4(){
        String hoTen = "Nguyen Van A";
        System.out.printf("Ten toi la \"%s\" va toi doc than", hoTen); //%s để in ra chuỗi đã khai báo
    }

//    ------------------------------------------------------------------------------------------------
//    Question 5:
//    Lấy thời gian bây giờ và in ra theo định dạng sau:
//            24/04/2020 11h:16p:20s
    public void Question5(){
        Date date = new Date();
        String fomat = "dd/MM/yyyy hh:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(fomat);
        String kq = simpleDateFormat.format(date);
        System.out.printf("%s",kq);
    }

//    ------------------------------------------------------------------------------------------------
//    Question 6:
//    In ra thông tin account (như Question 8 phần FOREACH) theo định dạng
//    table (giống trong Database)
    public void Question6(Account[] accounts) {
        System.out.printf("|%-20s|%-20s|%-20s|\n", "Email", "Fullname", "DepartmentName");
        for (Account account : accounts) {
            System.out.printf("|%-20s|%-20s|%-20s|\n",account.email,account.fullName,account.departmentId.departmentName);
        }


    }
}
