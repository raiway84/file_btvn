import java.util.Date;

public class Program {
    public static void main(String[] args) {
//        1.Khởi tạo Department
        Department department1 = new Department();
        department1.departmentId = 1;
        department1.departmentName = "A";
        System.out.println("1.Department");
        System.out.println("departmentId: "+ department1.departmentId + " departmentName: " + department1.departmentName);

//        2.Khởi tạo Position
        Position position1 = new Position();
        position1.positionId = 1;
        position1.positionName = PositionName.Dev;
        System.out.println("2.Position");
        System.out.println("positionId: "+ position1.positionId + " positionName: " + position1.positionName);

//        3.Khởi tạo Account
        Account account1 = new Account();
        account1.accountId = 1;
        account1.email = "abc@gmail.com";
        account1.userName = "a1";
        account1.fullName = "Nguyen Van A";
        account1.departmentId = 1;
        account1.positionId = 1;
        account1.createDate = new Date();
        System.out.println("3.Account");
        System.out.println("accountId: "+ account1.accountId + " email: " + account1.email);

//        4.Khởi tạo Group
        Group group1 = new Group();
        group1.groupId = 1;
        group1.groupName = "A1";
        group1.creatorId = 1;
        group1.createDate = new Date();
        System.out.println("4.Group");
        System.out.println("groupId: "+ group1.groupId + " groupName: " + group1.groupName);

//        5.Khởi tạo GroupAccount
        GroupAccount groupAccount1 = new GroupAccount();
        groupAccount1.groupId = 1;
        groupAccount1.accountId = 1;
        groupAccount1.joinDate = new Date();
        System.out.println("5.GroupAccount");
        System.out.println("groupId: "+ groupAccount1.groupId + " accountId: " + groupAccount1.accountId);

//        6.Khởi tạo TypeQuestion
        TypeQuestion typeQuestion1 = new TypeQuestion();
        typeQuestion1.typeId = 1;
        typeQuestion1.typeName = TypeName.Essay;
        System.out.println("6.TypeQuestion");
        System.out.println("typeId: "+ typeQuestion1.typeId + " typeName: " + typeQuestion1.typeName);

//        7.Khởi tạo CategoryQuestion
        CategoryQuestion categoryQuestion1 = new CategoryQuestion();
        categoryQuestion1.categoryId = 1;
        categoryQuestion1.categoryName = CategoryName.Java;
        System.out.println("7.CategoryQuestion");
        System.out.println("categoryId: "+ categoryQuestion1.categoryId + " categoryName: " + categoryQuestion1.categoryName);

//        8.Khởi tạo Question
        Question question1 = new Question();
        question1.questionId = 1;
        question1.content = "Trái đất hình gì?";
        question1.categoryId = 1;
        question1.typeId = 1;
        question1.creatorId = 1;
        question1.createDate = new Date();
        System.out.println("8.Question");
        System.out.println("Content: "+ question1.content);

//        9.Khởi tạo Answer
        Answer answer1 = new Answer();
        answer1.answerId = 1;
        answer1.content = "Trái đất hình cầu";
        answer1.questionId = 1;
        System.out.println("9.Answer");
        System.out.println("Content: "+ answer1.content);

//        10.Khởi tạo Exam
        Exam exam1 = new Exam();
        exam1.examId = 1;
        exam1.code = "AB1";
        exam1.title = "ExamFinal";
        exam1.categoryId = 1;
        exam1.duration = "90 phút";
        exam1.creatorId = 1;
        exam1.createDate = new Date();
        System.out.println("10.Exam");
        System.out.println("Duration: "+ exam1.duration);

//        11.Khởi tạo ExamQuestion
        ExamQuestion examQuestion1 = new ExamQuestion();
        examQuestion1.examId = 1;
        examQuestion1.questionId = 1;
        System.out.println("11.ExamQuestion");
        System.out.println("examId: "+ examQuestion1.examId);
    }
}
