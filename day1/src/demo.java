// Java là ngôn ngữ lập trình hướng đối tượng(có đặc điểm và hành động)
// Các đặc điểm... : là thuộc tính trong 1 class
// Các hành động là các phương thức(hàm, method)
//  - hàm main: dùng để chạy
//  - và các method thường
public class demo {

    // public: khả năng truy cập của class(mức truy cập cao nhất,
    // toàn project có thể truy cập đc)
    public static void main(String[] args) {
//        Nội dung code
        int number = 3;
//        1.kiểu dữ liệu nguyên thủy (primitive) - 8 kiểu dữ liệu
//        - Kiểu dữ liệu dạng logic: boolean(true/false)
        boolean logic = false;
//        - Kiểu dữ liệu dạng ký tự: char
        char type3 = 'a';
//        - Kiểu dữ liệu số nguyên: byte, short, int, long
        byte number1 = 120; // chiếm 1byte
        short number2 = 5;  // chiếm 2byte
        int number3 = 10; // chiếm 4byte
        long number4 = 2000; // chiếm 8byte

        int number5 = 15;
        int number6 = number3 + number5;
        int number7 = type3 + 5;
//        -Kiểu dữ liệu số thực: float(4byte), double(8byte)
        float number8 = 9.2f;
        double number9 = 8.5;
//         -Kiểu dữ liệu chuỗi
        String chuoi = "Hello";
        System.out.println(chuoi);
    }
//    method thường
    public void abc(){
    }
}
