public class demoArray {
    public static void main(String[] args) {
//        Khai báo 1 danh sách số int
        int[] numbers = {1, 3, 5, 7};

//        System.out.println(numbers[0]); // in ra số ở vị trí thứ 0
//        System.out.println(numbers.length); // in ra số phần tử có trong danh sách

        float[] number1 = new float[5]; // tạo ra array trống có thể chứa tối đa 5 phàn tử
//        Gán giá trị cho các ô trống
        number1[0] = 1.1f;
        System.out.println(number1[0]);
    }
}
