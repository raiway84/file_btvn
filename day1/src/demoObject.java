import java.util.Date;

public class demoObject {
    public static void main(String[] args) {
        System.out.println("Test");

//        (Kiểu dữ liệu) (tên biến) =( khởi tạo/ gán giá trị cho biến )
//        Sự khác nhau giữa Object và primitive
//          Object khởi tạo giá trị / primitive gán giá trị

        Student student1 = new Student(); //Khởi tạo giá trị cho biến student1
        // có kiểu dữ liệu là Student, giá trị là new Student()
//        student1.id = 1;
//        student1.msv = "ABC123";
//        student1.fullName = "Nguyen Van A";
//        student1.birthDay = new Date();


//        Bài toán in ra thông tin msv của student1
        System.out.println(student1.diem);
    }
}
